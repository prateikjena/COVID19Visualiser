import React from 'react';
import { Line ,Bar } from 'react-chartjs-2';
import { connect } from "react-redux";

class LineGraph extends React.Component<any,any>{
  render(){
    // console.log(this.props.stateDetail)
    let sc:any = this.props.stateDetail.stateCode.toLowerCase()
    let dataJson:any = []
    dataJson["confirmed"] = []
    dataJson["recovered"] = []
    dataJson["deceased"] = []
    dataJson["stateCode"] = sc
    dataJson["date"] = []
    // console.log(this.props.data.dailystatedata.states_daily)
    let k = this.props.data.dailystatedata.states_daily.map((key:any,value:any)=>{
      if(key["status"]==="Confirmed"){
          dataJson["confirmed"].push(Math.abs(key[sc]))
          dataJson["date"].push(key["date"])
      }
      else if(key["status"]==="Recovered" ){
        dataJson["recovered"].push(Math.abs(key[sc]))
      }
      else if(key["status"]==="Deceased"){
        dataJson["deceased"].push(Math.abs(key[sc]))
      }
    })

    const state = {
      labels: dataJson["date"],
      datasets: [
        {
          label: 'Confirmed',
          fill: false,
          fillColor: "rgba(151,187,205,0.2)",
          backgroundColor:"red",
          borderColor: 'red',
          borderWidth: 5,
          hoverBorderWidth:6,
          hoverBackgroundColor	:"red",
          hoverBorderColor	:"orange",
          data: dataJson["confirmed"]
        },
        {
          label: 'Recovered',
          fill: false,
          fillcolor: 'rgb(40, 167, 69)',
          borderColor: 'rgb(40, 167, 69)',
          borderWidth: 5,
          data: dataJson["recovered"],
          backgroundColor:'rgb(40, 167, 69)',
          pointHoverBorderColor	:"brown",
        },
        {
          label: 'Deceased',
          fill: false,
          fillcolor: 'blue',
          borderColor: 'blue',
          borderWidth: 5,
          data: dataJson["deceased"],
          backgroundColor:'blue',
          pointBorderColor:'rgba(0,0,0,0)',
          hoverBorderWidth:3,
          pointHoverBorderColor	:"purple",
        }
      ]
    }
    return(
      <div>
        <Bar
          data={state}
          options={{
            title:{
              display:true,
              text:'Covid - 19 '+this.props.stateDetail.state+' ( Daily Cases )',
              fontSize:20
            },
            tooltips: {
                  callbacks: {
                    label: function(tooltipItem:any, data:any) {
                        return " "+data.datasets[tooltipItem.datasetIndex].label+" : "+parseInt(tooltipItem.value).toLocaleString();
                    }
                  },
                  mode: 'index',//nearest index
                  intersect:false
                },
            legend:{
              display:true,
              position:'right'
            }
          }}
        />
      </div>
    )
  }
}

const mapStateToProps = (state: any) => {
  return {
    data: state.data
  };
}

export default connect(mapStateToProps)(LineGraph);
