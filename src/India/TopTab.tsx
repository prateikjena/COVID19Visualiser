import React , { Component } from "react";
import { Grid ,Typography,Paper } from "@material-ui/core";
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import moment from "moment";
import AssessmentIcon from '@material-ui/icons/Assessment';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

class TopTab extends Component<any,any>{
  constructor(props:any){
    super(props);
    this.state = {
      isClicked:false
    }
  }
  render(){
    console.log(this.props);
    // "state":key.state,
    // "stateCode":key.statecode,
    // "confirmed":parseInt(key.confirmed),
    // "active":parseInt(key.active),
    // "recovered":parseInt(key.recovered),
    // "deaths":parseInt(key.deaths),
    // "deltaconfirmed":Math.abs(parseInt(key.deltaconfirmed)),
    // "deltarecovered":parseInt(key.deltarecovered),
    // "deltadeaths":parseInt(key.deltadeaths),
    // "lastupdatedtime":key.lastupdatedtime
    return(
      <Grid
      container
      direction={"row"}
      spacing={2}
      >
      <Grid item xs={12}>
        <Paper elevation={3} >
          <Grid style={{padding:"10px",background:"#2196f3",borderRadius:"5px",color:"white"}}>
            <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h5"}>India Report</Typography>
          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={12}>
        <Paper style={{padding:"10px"}} elevation={3} >
          <Grid>
            <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>Last Updated</Typography>
          </Grid>
          <Grid>
            <Typography style={{fontWeight:"bold",color:"blue"}} variant={"caption"}>
            {
              (this.props.data[0]!==undefined)?this.props.data[0].lastupdatedtime:''
            }
            </Typography>
          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={isWidthUp("sm",this.props.width)?6:12}>
        <Paper style={{padding:"10px"}} elevation={3} >
          <Grid>
            <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>Total Cases</Typography>
          </Grid>
          <Grid>
            <Typography style={{fontWeight:"bold",color:"blue"}} variant={"caption"}>
            {
              (this.props.data[0]!==undefined)?this.props.data[0].confirmed.toLocaleString():""
            }
            </Typography>
            <Typography style={{fontWeight:"bold",color:"red"}} variant={"caption"}>
            {
              (this.props.data[0]!==undefined)?"        +"+this.props.data[0].deltaconfirmed.toLocaleString()+" ⬆":''
            }
            </Typography>
          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={isWidthUp("sm",this.props.width)?6:12}>
        <Paper style={{padding:"10px"}} elevation={3}>
        <Grid>
            <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>Active</Typography>
          </Grid>
          <Grid>
            <Typography style={{fontWeight:"bold",color:"blue"}} variant={"caption"}>
            {
                (this.props.data[0]!==undefined)?this.props.data[0].active.toLocaleString():''
            }
            </Typography>

          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={isWidthUp("sm",this.props.width)?6:12}>
        <Paper style={{padding:"10px"}} elevation={3}>
          <Grid>
            <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>Recovered</Typography>
          </Grid>
          <Grid>
            <Typography style={{fontWeight:"bold",color:"blue"}} variant={"caption"}>
            {
                (this.props.data[0]!==undefined)?this.props.data[0].recovered.toLocaleString():''
            }
            </Typography>
            <Typography style={{fontWeight:"bold",color:"red"}} variant={"caption"}>
            {
                (this.props.data[0]!==undefined)?"       +"+this.props.data[0].deltarecovered.toLocaleString()+" ⬆":''
            }
            </Typography>
          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={isWidthUp("sm",this.props.width)?6:12}>
        <Paper style={{padding:"10px"}} elevation={3}>
          <Grid>
            <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>Deceased</Typography>
          </Grid>
          <Grid>
            <Typography style={{fontWeight:"bold",color:"blue"}} variant={"caption"}>
            {
                (this.props.data[0]!==undefined)?this.props.data[0].deaths.toLocaleString():''
            }
            </Typography>
            <Typography style={{fontWeight:"bold",color:"red"}} variant={"caption"}>
            {
                (this.props.data[0]!==undefined)?"       +"+this.props.data[0].deltadeaths.toLocaleString()+" ⬆":''
            }
            </Typography>
          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={12}>
        <Grid
          container
          justify={this.state.isClicked?"flex-start":"flex-end"}
          >
            <Grid item>
            {
              !this.state.isClicked?
              <Link style={{textDecoration:"none"}} to="/india/statewisereport">
                <Button
                    onClick = {()=>{
                      this.setState({
                        isClicked:!this.state.isClicked
                      })
                    }}
                    style={{
                      width:"100%"
                    }}
                    variant="contained"
                    color="primary"
                    startIcon={<AssessmentIcon/>}
                    >
                    <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>StateWise Report</Typography>
                </Button>
              </Link>
              :
              <Link style={{textDecoration:"none"}} to="/india">
                <Button
                    onClick = {()=>{
                      this.setState({
                        isClicked:!this.state.isClicked
                      })
                    }}
                    style={{
                      width:"100%"
                    }}
                    variant="contained"
                    color="primary"
                    startIcon={<ArrowBackIosIcon/>}
                    >
                    <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>Back</Typography>
                </Button>
              </Link>
            }
            </Grid>
        </Grid>
       </Grid>
      </Grid>
    )
  }
}

export default withWidth()(TopTab)
