export const SETINDIADATA = 'SETINDIADATA';
export const SETCOUNTRYDATA = 'SETCOUNTRYDATA';
export const SETGLOBALDATA = 'SETGLOBALDATA';
export const SETGRAPHGLOBALDATA = 'SETGRAPHGLOBALDATA';
export const SETSTATEDATA = 'SETSTATEDATA';
export const SETDAILYSTATEDATA = 'SETDAILYSTATEDATA';

export const setCountryData = (data: any) => {
  return{
    type:SETCOUNTRYDATA,
    data:data
  }
}
export const setIndiaData = (data: any) => {
  return{
    type:SETINDIADATA,
    data:data
  }
}
export const setStateData = (data: any) => {
  return{
    type:SETSTATEDATA,
    data:data
  }
}
export const setGlobalData = (data: any) => {
  return{
    type:SETGLOBALDATA,
    data:data
  }
}
export const setGraphGlobalData = (data: any) => {
  return{
    type:SETGRAPHGLOBALDATA,
    data:data
  }
}
export const setDailyStateData = (data: any) => {
  return{
    type:SETDAILYSTATEDATA,
    data:data
  }
}
