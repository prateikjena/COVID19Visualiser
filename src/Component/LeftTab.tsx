import React from 'react';
import { Grid,Paper } from '@material-ui/core';
import { Typography } from '@material-ui/core';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import moment from 'moment'
import Avatar from '@material-ui/core/Avatar';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { setGlobalData,setGraphGlobalData } from "./../appStore/actions";
import { connect } from 'react-redux';
import AssessmentIcon from '@material-ui/icons/Assessment';
import HomeIcon from '@material-ui/icons/Home';
import GitHubIcon from '@material-ui/icons/GitHub';
import IconButton from '@material-ui/core/IconButton';
import { SvgIcon } from '@material-ui/core';
import Chip from '@material-ui/core/Chip';

class LeftTab extends React.Component<any,any>{
  constructor(props:any){
    super(props);
  }
  componentDidMount(){
    fetch("https://corona.lmao.ninja/v2/all?yesterday=false")
	.then((res:any)=>{
     return res.json()
    })
    .then((d:any)=>{
      this.props.setGlobalData(d)
    })
    // https://corona.lmao.ninja/v2/historical/all
    fetch("https://corona.lmao.ninja/v2/historical/all")
    .then((res:any)=>{
     return res.json()
    })
    .then((d:any)=>{
      this.props.setGraphGlobalData(d)
    })
  }
  render(){
    // console.log(this.props.data.global)
    return(
      <Grid item xs={12}>
          <Paper style={{padding:"10px"}} elevation={3} >
             <Grid
                container
                spacing={2}
                justify={"center"}
                direction={isWidthUp("sm",this.props.width)?"column":"row"}
                >
                <Grid item xs={12}>
                  <Grid
                    container
                    spacing={2}
                    direction={isWidthUp("sm",this.props.width)?"column":"row"}
                    justify={isWidthUp("sm",this.props.width)?"center":"space-between"}
                    alignItems={"center"}
                    >
                    <Grid item>
                      <Typography
                        variant={"subtitle2"}
                        style={{fontWeight:"bold"}}
                        component={"h5"}
                        >
                        Covid - 19 Cases
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Grid
                        container
                        alignItems={"center"}
                        justify={"center"}
                        direction={"column"}
                        >
                        <Grid item xs={12}>
                          <Typography
                            variant={"subtitle2"}
                            style={{fontWeight:"bold"}}
                            component={"h5"}
                            >
                            <IconButton onClick={()=>{window.open('https://github.com/prateikjena')}}>
                              <SvgIcon width="280px" height="272px" viewBox="0 0 280 272" version="1.1" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid"><g>
                                <path d="M128.07485,236.074667 L128.07485,236.074667 L175.17885,91.1043048 L80.9708495,91.1043048 L128.07485,236.074667 L128.07485,236.074667 Z" fill="#E24329"></path>
                                <path d="M128.07485,236.074423 L80.9708495,91.104061 L14.9557638,91.104061 L128.07485,236.074423 L128.07485,236.074423 Z" fill="#FC6D26"></path>
                                <path d="M14.9558857,91.1044267 L14.9558857,91.1044267 L0.641828571,135.159589 C-0.663771429,139.17757 0.766171429,143.57955 4.18438095,146.06275 L128.074971,236.074789 L14.9558857,91.1044267 L14.9558857,91.1044267 Z" fill="#FCA326"></path>
                                <path d="M14.9558857,91.1045486 L80.9709714,91.1045486 L52.6000762,3.79026286 C51.1408762,-0.703146667 44.7847619,-0.701927619 43.3255619,3.79026286 L14.9558857,91.1045486 L14.9558857,91.1045486 Z" fill="#E24329"></path>
                                <path d="M128.07485,236.074423 L175.17885,91.104061 L241.193935,91.104061 L128.07485,236.074423 L128.07485,236.074423 Z" fill="#FC6D26"></path>
                                <path d="M241.193935,91.1044267 L241.193935,91.1044267 L255.507992,135.159589 C256.813592,139.17757 255.38365,143.57955 251.96544,146.06275 L128.07485,236.074789 L241.193935,91.1044267 L241.193935,91.1044267 Z" fill="#FCA326"></path>
                                <path d="M241.193935,91.1045486 L175.17885,91.1045486 L203.549745,3.79026286 C205.008945,-0.703146667 211.365059,-0.701927619 212.824259,3.79026286 L241.193935,91.1045486 L241.193935,91.1045486 Z" fill="#E24329"></path>
                              </g></SvgIcon>
                            </IconButton>
                          </Typography>
                        </Grid>
                        <Grid item xs={12}>
                        {isWidthUp("sm",this.props.width)?(
                          <Typography
                            style={{fontWeight:"bold",fontSize:8}}
                            variant={"overline"}
                            gutterBottom
                            >
                            Prateek Jena
                          </Typography>):''}
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid item xs={12}>
                  <Link style={{textDecoration:"none"}} to="/">
                    <Button
                        style={{
                          width:"100%"
                        }}
                        variant="contained"
                        color="primary"
                        startIcon={<HomeIcon/>}
                        >
                        <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>Home</Typography>
                   </Button>
                  </Link>
                </Grid>
                <Grid item xs={12}>
                  <Paper style={{padding:"10px"}} elevation={3} >
                    <Grid>
                      <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>Last Updated</Typography>
                    </Grid>
                    <Grid>
                      <Typography style={{fontWeight:"bold",color:"blue"}} variant={"caption"}>
                      {
                        (this.props.data.global!==null && this.props.data.global.updated!==undefined)?moment(this.props.data.global.updated).format("DD MMM YYYY HH:MM:SS"):''
                      }
                      </Typography>
                    </Grid>
                  </Paper>
                </Grid>
                <Grid item xs={isWidthUp("sm",this.props.width)?12:6}>
                  <Paper style={{padding:"10px"}} elevation={3} >
                    <Grid>
                      <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>Total Cases</Typography>
                    </Grid>
                    <Grid>
                      <Typography style={{fontWeight:"bold",color:"blue"}} variant={"caption"}>
                      {
                        (this.props.data.global!==null && this.props.data.global.cases!==undefined)?this.props.data.global.cases.toLocaleString():''
                      }
                      </Typography>
                      <Typography style={{fontWeight:"bold",color:"red"}} variant={"caption"}>
                      {
                        (this.props.data.global!==null && this.props.data.global.todayCases!==undefined)?"   +"+this.props.data.global.todayCases.toLocaleString()+" ⬆":''
                      }
                      </Typography>
                    </Grid>
                  </Paper>
                </Grid>
                <Grid item xs={isWidthUp("sm",this.props.width)?12:6}>
                  <Paper style={{padding:"10px"}} elevation={3}>
                  <Grid>
                      <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>Active</Typography>
                    </Grid>
                    <Grid>
                      <Typography style={{fontWeight:"bold",color:"blue"}} variant={"caption"}>
                      {
                          (this.props.data.global!==null && this.props.data.global.active!==undefined)?this.props.data.global.active.toLocaleString():''
                      }
                      </Typography>

                    </Grid>
                  </Paper>
                </Grid>
                <Grid item xs={isWidthUp("sm",this.props.width)?12:6}>
                  <Paper style={{padding:"10px"}} elevation={3}>
                    <Grid>
                      <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>Recovered</Typography>
                    </Grid>
                    <Grid>
                      <Typography style={{fontWeight:"bold",color:"blue"}} variant={"caption"}>
                      {
                          (this.props.data.global!==null && this.props.data.global.recovered!==undefined)?this.props.data.global.recovered.toLocaleString():''
                      }
                      </Typography>
                    </Grid>
                  </Paper>
                </Grid>
                <Grid item xs={isWidthUp("sm",this.props.width)?12:6}>
                  <Paper style={{padding:"10px"}} elevation={3}>
                    <Grid>
                      <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>Deceased</Typography>
                    </Grid>
                    <Grid>
                      <Typography style={{fontWeight:"bold",color:"blue"}} variant={"caption"}>
                      {
                          (this.props.data.global!==null && this.props.data.global.deaths!==undefined)?this.props.data.global.deaths.toLocaleString():''
                      }
                      </Typography>
                      <Typography style={{fontWeight:"bold",color:"red"}} variant={"caption"}>
                      {
                          (this.props.data.global!==null && this.props.data.global.todayDeaths!==undefined)?"   +"+this.props.data.global.todayDeaths.toLocaleString()+" ⬆":''
                      }
                      </Typography>
                    </Grid>
                  </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Grid
                      container
                      direction={"row"}
                      spacing={2}
                      >
                      <Grid item xs={isWidthUp("sm",this.props.width)?12:6}>
                        <Link style={{textDecoration:"none"}} to="/India">
                          <Button
                              style={{
                                width:"100%"
                              }}
                              variant="contained"
                              color="primary"
                              startIcon={<img width={25} height={20} src="https://disease.sh/assets/img/flags/in.png" alt="in"/>}
                              >
                              <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>India Report</Typography>
                         </Button>
                        </Link>
                      </Grid>
                      <Grid item xs={isWidthUp("sm",this.props.width)?12:6}>
                        <Link style={{textDecoration:"none"}} to="/global">
                          <Button
                              style={{
                                width:"100%"
                              }}
                              variant="contained"
                              color="primary"
                              startIcon={<AssessmentIcon/>}
                              >
                              <Typography variant={"caption"} style={{fontWeight:"bold"}} component={"h6"}>Global Report</Typography>
                         </Button>
                        </Link>
                      </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container alignItems={"center"} justify={"center"}>
                      <a href="https://github.com/prateikjena/covid19visualization">
                        covid19visualization
                      </a>
                    </Grid>
                </Grid>
            </Grid>
          </Paper>
      </Grid>
    )
  }
}

const mapStateToProps = (state: any) => {
  return {
    data: state.data
  };
}

const mapDispatchToProps = {
  setGlobalData,
  setGraphGlobalData
};

export default withWidth()(connect(mapStateToProps,mapDispatchToProps)(LeftTab));
