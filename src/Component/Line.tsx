import React from 'react';
import { Box, Grid } from '@material-ui/core';
import { Line } from 'react-chartjs-2';
import moment from 'moment';
import { LoadIndicator } from 'devextreme-react/load-indicator';

class LineGraph extends React.Component<any,any> {
  chart: any;
  country = "India";
    constructor(props:any){
        super(props);
        this.state={
            data:[],
            isLoading:true,
            iso2:this.props.countryDetail.iso2
        }
    }

    componentDidMount(){
        fetch("https://api.covid19api.com/total/dayone/country/"+this.state.iso2)
        .then((res:any)=>res.json())
        .then((data:any)=>{
          // console.log(data)
            this.setState({
                data:data,
                isLoading:false
            })
        })
    }
    render(){
      const da:any = []
      da["label"] = []
      da["data"] = []
      da["data"]["Confirmed"] = []
      da["data"]["Recovered"] = []
      da["data"]["Active"] = []
      da["data"]["Deceased"] = []
      let d = (this.state.data!==undefined && this.state.data!==[] && this.state.data!=="code=404, message=Not Found, internal=<nil>")? this.state.data.map((key:any,value:any)=>{
        da["label"].push(moment(key.Date).format("DD MMM"));
        da["data"]["Confirmed"].push({
          "x":key.Date,
          "y":key.Confirmed
        });
        da["data"]["Recovered"].push({
          "x":key.Date,
          "y":key.Recovered
        });
        da["data"]["Active"].push({
          "x":key.Date,
          "y":key.Active
        });
        da["data"]["Deceased"].push({
          "x":key.Date,
          "y":key.Deaths
        });
      }):''
    //  console.log(da)
      const state = {
        labels: da["label"],
        datasets: [
          {
            label: 'Confirmed',
            fill: false,
            pointRadius	:2,
            pointStyle:'dash',
            fillColor: "rgba(151,187,205,0.2)",
            pointBackgroundColor:"red",
            borderColor: 'red',
            borderWidth: 3,
            pointHoverBackgroundColor	:"red",
            pointHoverBorderColor	:"red",
            pointBorderColor:'rgba(0,0,0,0)',
            data: da["data"]["Confirmed"]
          },
          {
            label: 'Active',
            fill: false,
            pointRadius	:2,
            pointStyle:'dash',
            fillColor: 'rgb(108, 117, 125)',
            borderColor: 'rgb(108, 117, 125)',
            pointBackgroundColor:'rgb(108, 117, 125)',
            borderWidth: 3,
            data: da["data"]["Active"],
            pointBorderColor:'rgba(0,0,0,0)',
          },
          {
            label: 'Recovered',
            fill: false,
            pointRadius	:2,
            pointStyle:'dash',
            fillcolor: 'rgb(40, 167, 69)',
            borderColor: 'rgb(40, 167, 69)',
            borderWidth: 3,
            data: da["data"]["Recovered"],
            pointBackgroundColor:'rgb(40, 167, 69)',
            pointBorderColor:'rgba(0,0,0,0)',
          },
          {
            label: 'Deceased',
            fill: false,
            pointRadius	:2,
            pointStyle:'dash',
            fillcolor: 'blue',
            borderColor: 'blue',
            borderWidth: 3,
            data: da["data"]["Deceased"],
            pointBackgroundColor:'blue',
            pointBorderColor:'rgba(0,0,0,0)',
          }
        ]
      }
      return(
        <Grid
          container
          direction={"row"}
          alignItems={"center"}
          >
          <Grid item xs={6}></Grid>
          {
          !this.state.isLoading?
          <Grid item xs={12}>
            <Box>
                <Line
                  data={state}
                  options={{
                    responsive:true,
                    title:{
                      display:true,
                      text:'Covid - 19 Cases ( '+this.props.countryDetail.country+' )',
                      fontSize:20
                    },
                    legend:{
                      display:true,
                      position:'right'
                    },
                    tooltips: {
                      callbacks: {
                        label: function(tooltipItem:any, data:any) {
                          // console.log(tooltipItem)
                            return " "+data.datasets[tooltipItem.datasetIndex].label+" : "+parseInt(tooltipItem.value).toLocaleString();
                        }
                      },
                      mode: 'index',//nearest index
                      intersect:false
                    }
                  }}
                />
              </Box>
            </Grid>:<Grid item xs={4}><LoadIndicator/></Grid>
          }
          <Grid item xs={2}></Grid>
        </Grid>
      )
    }
	}

export default LineGraph ;
