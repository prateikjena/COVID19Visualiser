import React from 'react';
import './App.css';
import { Route,  Link } from 'react-router-dom';
import { Grid,Paper } from '@material-ui/core';
import LeftTab from './Component/LeftTab';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import Global from "./Global"
import India from './India'
import PieGraph from './Component/Pie'

class App extends React.Component<any,{data:any}>{
  constructor(props:any){
    super(props);
  }

  render(){
    return(
        <Grid
          container
          direction={isWidthUp("sm",this.props.width)?"row":"column"}
          alignItems={"flex-start"}
          spacing={1}
          >
          <Grid item xs={isWidthUp("sm",this.props.width)?2:12}>
            <Route path="/">
              <LeftTab/>
            </Route>
          </Grid>
          <Grid item xs={isWidthUp("sm",this.props.width)?10:12}>
            <Route path="/" exact component={PieGraph}/>
            <Route path="/global" exact component={Global}/>
            <Route path="/india">
              <India/>
            </Route>
          </Grid>
        </Grid>
    );
  }
}


export default withWidth()(App);
